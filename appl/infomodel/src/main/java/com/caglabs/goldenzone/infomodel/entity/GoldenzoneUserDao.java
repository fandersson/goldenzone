package com.caglabs.goldenzone.infomodel.entity;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * DAO for accessing {@link GoldenzoneUser}:s.
 */
public class GoldenzoneUserDao {
    @PersistenceContext(unitName = "GoldenzonePU")
    public EntityManager entityManager;

    /**
     * Get System user by name.
     * @param name User name
     * @return User object or null if a user with that name does not exist
     */
    public GoldenzoneUser getSystemUserByName(String name) {
        try {
            return entityManager.createNamedQuery(GoldenzoneUser.GET_BY_NAME, GoldenzoneUser.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void save(GoldenzoneUser u) {
        entityManager.persist(u);
    }
}
